﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDM.Util
{
    public static class GroupMails
    {
        public static string QualityGroupMail { get; private set; } = "zaneta.belas@gmail.com";
        public static string ProductionGroupMail { get; private set; } = "piotr.p.majewski@gmail.com";
        public static string Technician { get; private set; } = "piotr.p.majewski@gmail.com";
    }
}