﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.IO;

namespace OTDM.Util
{
    public static class MailSender
    {
        public static void SendMail(string sender, string receivers, string subject, string content)
        {
            var body = string.Empty;

            using (var reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/MailTemplates/email.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{#Content#}", content);
            
            using( var mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(sender);
                mailMessage.To.Add(receivers);
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                var smtpServer = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new NetworkCredential("omegatestingdevices@gmail.com", "Admin123!"),
                    EnableSsl = true
                };

                smtpServer.Send(mailMessage);

            }
        }

    }

}