﻿$(document).ready(function () {

    $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
        console.log('show tab');
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
    });


 $('.dtable').DataTable({
        responsive: {
            details: {
                renderer: function (api, rowIdx, columns) {
                    var data = $.map(columns, function (col, i) {
                        return col.hidden ?
                            '<tr style="border-bottom: solid 1px #d3d3d3; " data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                            '<td style="padding: 5px; font-weight: bold; white-space:pre-wrap;">' + col.title + ':' + '</td> ' +
                            '<td style="padding: 5px; white-space:pre-wrap; width:100%; display: inline-block;">' + col.data + '</td>' +
                            '</tr>' :
                            '';
                    }).join('');

                    return data ?
                        $('<table/>').append(data) :
                        false;
                }
            }
        }
    });
});

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function () {
    $('.dtable').on("click",".js-delete", function () {
        var button = $(this);

        bootbox.confirm("Czy jestes pewien, że chcesz usunąć ten tester?", function (result) {
            if (result) {
                $.ajax({
                    url: "/api/devices/" + button.attr("id"),
                    method: "DELETE",
                    success: function () {
                        button.parents("tr").remove();
                    }
                });
            }
        });
    });
   
});