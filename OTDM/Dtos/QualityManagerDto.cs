﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace OTDM.Dtos
{
    public class QualityManagerDto 
    {

            public int Id { get; set; }

            [Required]
            public string FirstName { get; set; }

            [Required]
            public string LastName { get; set; }

            [Phone]
            public string PhoneNumber { get; set; }

            [Required]
            [EmailAddress]
            public string MailAddress { get; set; }

            public string Manager { get; set; }
    }
}