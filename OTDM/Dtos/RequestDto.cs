﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OTDM.Models;
using System.ComponentModel.DataAnnotations;

namespace OTDM.Dtos
{
    public class RequestDto
    {

        public int Id { get; set; }

        [Required]
        public int RequesterId { get; set; }

        [Required]
        public string AffectedUser { get; set; }

        [Required]
        public int RequestedDeviceId { get; set; }

        [Required]
        public DateTime StartingDate { get; set; }
    }
}