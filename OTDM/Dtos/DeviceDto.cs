﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OTDM.Models;
using System.ComponentModel.DataAnnotations;
using OTDM.Models.Validations;

namespace OTDM.Dtos
{
    public class DeviceDto
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        //[SringChecker]
        public string Code { get; set; }

        [Required]
        public int DeviceCategoryId { get; set; }

        public DeviceCategoryDto DeviceCategory { get; set; }

        public bool IsValidated { get; set; }

        [Required]
        public DateTime NextValidationDate { get; set; }
    }
}