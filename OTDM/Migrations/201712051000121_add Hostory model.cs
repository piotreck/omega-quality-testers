namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addHostorymodel : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Requests", newName: "Histories");
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequesterId = c.String(nullable: false, maxLength: 128),
                        ProductionUser = c.String(nullable: false),
                        OrderId = c.String(nullable: false),
                        RequestedDeviceId = c.Int(nullable: false),
                        StartingDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.RequesterId)
                .Index(t => t.RequestedDeviceId);
            
            DropColumn("dbo.Histories", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Histories", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            DropIndex("dbo.Requests", new[] { "RequestedDeviceId" });
            DropIndex("dbo.Requests", new[] { "RequesterId" });
            DropTable("dbo.Requests");
            RenameTable(name: "dbo.Histories", newName: "Requests");
        }
    }
}
