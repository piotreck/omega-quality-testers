namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addevidenceandproblemmodels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Problems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeviceId = c.Int(nullable: false),
                        ReporterId = c.String(maxLength: 128),
                        ProblemDescription = c.String(),
                        ReportingDate = c.DateTime(nullable: false),
                        Status = c.String(),
                        ResolverId = c.String(maxLength: 128),
                        ResolutionDescription = c.String(),
                        ResolutionDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Devices", t => t.DeviceId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ReporterId)
                .ForeignKey("dbo.AspNetUsers", t => t.ResolverId)
                .Index(t => t.DeviceId)
                .Index(t => t.ReporterId)
                .Index(t => t.ResolverId);
            
            AddColumn("dbo.Requests", "Status", c => c.String());
            AddColumn("dbo.Requests", "Comment", c => c.String());
            AddColumn("dbo.Requests", "Discriminator", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Problems", "ResolverId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Problems", "ReporterId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Problems", "DeviceId", "dbo.Devices");
            DropIndex("dbo.Problems", new[] { "ResolverId" });
            DropIndex("dbo.Problems", new[] { "ReporterId" });
            DropIndex("dbo.Problems", new[] { "DeviceId" });
            DropColumn("dbo.Requests", "Discriminator");
            DropColumn("dbo.Requests", "Comment");
            DropColumn("dbo.Requests", "Status");
            DropTable("dbo.Problems");
        }
    }
}
