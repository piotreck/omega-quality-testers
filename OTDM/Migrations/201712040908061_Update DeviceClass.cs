namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDeviceClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Devices", "IsAvailable", c => c.Boolean(nullable: false));
            AddColumn("dbo.Devices", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Devices", "UserId");
            AddForeignKey("dbo.Devices", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Devices", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Devices", new[] { "UserId" });
            DropColumn("dbo.Devices", "UserId");
            DropColumn("dbo.Devices", "IsAvailable");
        }
    }
}
