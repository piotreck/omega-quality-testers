namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Requests", "RequesterId", "dbo.QualityManagers");
            DropIndex("dbo.Requests", new[] { "RequesterId" });
            AddColumn("dbo.Requests", "ProductionUser", c => c.String(nullable: false));
            AddColumn("dbo.Requests", "OrderId", c => c.String(nullable: false));
            AddColumn("dbo.Requests", "Requester_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Requests", "Requester_Id");
            AddForeignKey("dbo.Requests", "Requester_Id", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Requests", "AffectedUser");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Requests", "AffectedUser", c => c.String(nullable: false));
            DropForeignKey("dbo.Requests", "Requester_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Requests", new[] { "Requester_Id" });
            DropColumn("dbo.Requests", "Requester_Id");
            DropColumn("dbo.Requests", "OrderId");
            DropColumn("dbo.Requests", "ProductionUser");
            CreateIndex("dbo.Requests", "RequesterId");
            AddForeignKey("dbo.Requests", "RequesterId", "dbo.QualityManagers", "Id", cascadeDelete: true);
        }
    }
}
