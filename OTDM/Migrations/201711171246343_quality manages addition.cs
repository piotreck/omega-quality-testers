namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class qualitymanagesaddition : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Controllers", newName: "QualityManagers");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.QualityManagers", newName: "Controllers");
        }
    }
}
