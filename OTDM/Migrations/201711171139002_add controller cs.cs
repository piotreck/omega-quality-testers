namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcontrollercs : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Operators", newName: "Controllers");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Controllers", newName: "Operators");
        }
    }
}
