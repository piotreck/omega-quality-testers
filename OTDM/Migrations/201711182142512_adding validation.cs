namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingvalidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Devices", "Code", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Devices", "Code", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
