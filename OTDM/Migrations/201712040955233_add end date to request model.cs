namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addenddatetorequestmodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Requests", "EndDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Requests", "EndDate");
        }
    }
}
