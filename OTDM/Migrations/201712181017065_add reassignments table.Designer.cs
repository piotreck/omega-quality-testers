// <auto-generated />
namespace OTDM.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addreassignmentstable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addreassignmentstable));
        
        string IMigrationMetadata.Id
        {
            get { return "201712181017065_add reassignments table"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
