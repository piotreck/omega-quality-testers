namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usersviewcreation : DbMigration
    {
        public override void Up()
        {
            Sql("CREATE VIEW[dbo].[UsersView] AS SELECT Id, userName, FullName, Email, PhoneNumber FROM [AspNetUsers]");
        }
        
        public override void Down()
        {
        }
    }
}
