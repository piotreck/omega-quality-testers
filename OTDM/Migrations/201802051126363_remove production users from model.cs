namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeproductionusersfrommodel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Requests", "ProductionUser", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Requests", "ProductionUser", c => c.String(nullable: false));
        }
    }
}
