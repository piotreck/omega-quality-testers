namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingrequeststable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequesterId = c.Int(nullable: false),
                        AffectedUser = c.String(nullable: false),
                        RequestedDeviceId = c.Int(nullable: false),
                        StartingDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Devices", t => t.RequestedDeviceId, cascadeDelete: true)
                .ForeignKey("dbo.QualityManagers", t => t.RequesterId, cascadeDelete: true)
                .Index(t => t.RequesterId)
                .Index(t => t.RequestedDeviceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Requests", "RequesterId", "dbo.QualityManagers");
            DropForeignKey("dbo.Requests", "RequestedDeviceId", "dbo.Devices");
            DropIndex("dbo.Requests", new[] { "RequestedDeviceId" });
            DropIndex("dbo.Requests", new[] { "RequesterId" });
            DropTable("dbo.Requests");
        }
    }
}
