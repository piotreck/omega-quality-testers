namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class estimatedusagetimeadded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Requests", "EstimatedUsageTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Requests", "EstimatedUsageTime");
        }
    }
}
