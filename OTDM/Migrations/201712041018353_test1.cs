namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Requests", "Requester_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Requests", new[] { "Requester_Id" });
            DropColumn("dbo.Requests", "RequesterId");
            RenameColumn(table: "dbo.Requests", name: "Requester_Id", newName: "RequesterId");
            AlterColumn("dbo.Requests", "RequesterId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Requests", "RequesterId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Requests", "RequesterId");
            AddForeignKey("dbo.Requests", "RequesterId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Requests", "RequesterId", "dbo.AspNetUsers");
            DropIndex("dbo.Requests", new[] { "RequesterId" });
            AlterColumn("dbo.Requests", "RequesterId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Requests", "RequesterId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Requests", name: "RequesterId", newName: "Requester_Id");
            AddColumn("dbo.Requests", "RequesterId", c => c.Int(nullable: false));
            CreateIndex("dbo.Requests", "Requester_Id");
            AddForeignKey("dbo.Requests", "Requester_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
