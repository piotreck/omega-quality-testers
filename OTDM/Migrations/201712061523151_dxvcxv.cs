namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dxvcxv : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Histories", "DeviceManagerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Histories", "RequestedDeviceId", "dbo.Devices");
            DropForeignKey("dbo.Histories", "RequesterId", "dbo.AspNetUsers");
            DropIndex("dbo.Histories", new[] { "RequesterId" });
            DropIndex("dbo.Histories", new[] { "RequestedDeviceId" });
            DropIndex("dbo.Histories", new[] { "DeviceManagerId" });
            AlterColumn("dbo.Requests", "ProductionUser", c => c.String(nullable: false));
            AlterColumn("dbo.Requests", "OrderId", c => c.String(nullable: false));
            DropTable("dbo.Histories");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Histories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequesterId = c.String(maxLength: 128),
                        ProductionUser = c.String(),
                        OrderId = c.String(),
                        RequestedDeviceId = c.Int(nullable: false),
                        StartingDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        DeviceManagerId = c.String(maxLength: 128),
                        Status = c.String(),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.Requests", "OrderId", c => c.String());
            AlterColumn("dbo.Requests", "ProductionUser", c => c.String());
            CreateIndex("dbo.Histories", "DeviceManagerId");
            CreateIndex("dbo.Histories", "RequestedDeviceId");
            CreateIndex("dbo.Histories", "RequesterId");
            AddForeignKey("dbo.Histories", "RequesterId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Histories", "RequestedDeviceId", "dbo.Devices", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Histories", "DeviceManagerId", "dbo.AspNetUsers", "Id");
        }
    }
}
