namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addgiverandreceiver : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Requests", "GiverId", c => c.String(maxLength: 128));
            AddColumn("dbo.Requests", "ReceiverId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Requests", "GiverId");
            CreateIndex("dbo.Requests", "ReceiverId");
            AddForeignKey("dbo.Requests", "GiverId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Requests", "ReceiverId", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Logs", "DeviceManagerId");
            DropColumn("dbo.Logs", "EndDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Logs", "EndDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Logs", "DeviceManagerId", c => c.String());
            DropForeignKey("dbo.Requests", "ReceiverId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Requests", "GiverId", "dbo.AspNetUsers");
            DropIndex("dbo.Requests", new[] { "ReceiverId" });
            DropIndex("dbo.Requests", new[] { "GiverId" });
            DropColumn("dbo.Requests", "ReceiverId");
            DropColumn("dbo.Requests", "GiverId");
        }
    }
}
