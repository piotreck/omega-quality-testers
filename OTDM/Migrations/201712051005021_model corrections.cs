namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modelcorrections : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Histories", "DeviceManagerId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Histories", "DeviceManagerId");
            AddForeignKey("dbo.Histories", "DeviceManagerId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Histories", "DeviceManagerId", "dbo.AspNetUsers");
            DropIndex("dbo.Histories", new[] { "DeviceManagerId" });
            DropColumn("dbo.Histories", "DeviceManagerId");
        }
    }
}
