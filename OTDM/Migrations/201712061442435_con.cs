namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class con : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Histories", "RequesterId", "dbo.AspNetUsers");
            DropIndex("dbo.Histories", new[] { "RequesterId" });
            AlterColumn("dbo.Histories", "RequesterId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Histories", "ProductionUser", c => c.String());
            AlterColumn("dbo.Histories", "OrderId", c => c.String());
            CreateIndex("dbo.Histories", "RequesterId");
            AddForeignKey("dbo.Histories", "RequesterId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Histories", "RequesterId", "dbo.AspNetUsers");
            DropIndex("dbo.Histories", new[] { "RequesterId" });
            AlterColumn("dbo.Histories", "OrderId", c => c.String(nullable: false));
            AlterColumn("dbo.Histories", "ProductionUser", c => c.String(nullable: false));
            AlterColumn("dbo.Histories", "RequesterId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Histories", "RequesterId");
            AddForeignKey("dbo.Histories", "RequesterId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
