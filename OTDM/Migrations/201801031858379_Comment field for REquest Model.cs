namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CommentfieldforREquestModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Requests", "Comment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Requests", "Comment");
        }
    }
}
