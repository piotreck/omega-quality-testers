namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addstatusenumtoProblemandrequestmodels : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Problems", "Status");
            DropColumn("dbo.Requests", "Status");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Requests", "Status", c => c.String());
            AddColumn("dbo.Problems", "Status", c => c.String());
        }
    }
}
