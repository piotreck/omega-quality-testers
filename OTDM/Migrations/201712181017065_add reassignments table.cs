namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addreassignmentstable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Logs", "RequestId", "dbo.Requests");
            DropIndex("dbo.Logs", new[] { "RequestId" });
            CreateTable(
                "dbo.Reassignments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequestId = c.Int(nullable: false),
                        ReassignmentDate = c.DateTime(nullable: false),
                        ReassignedFrom = c.String(),
                        ReassignedTo = c.String(),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Requests", t => t.RequestId, cascadeDelete: true)
                .Index(t => t.RequestId);
            
            DropTable("dbo.Logs");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequestId = c.Int(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Reassignments", "RequestId", "dbo.Requests");
            DropIndex("dbo.Reassignments", new[] { "RequestId" });
            DropTable("dbo.Reassignments");
            CreateIndex("dbo.Logs", "RequestId");
            AddForeignKey("dbo.Logs", "RequestId", "dbo.Requests", "Id", cascadeDelete: true);
        }
    }
}
