namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullabledate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Problems", "ResolutionDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Problems", "ResolutionDate", c => c.DateTime(nullable: false));
        }
    }
}
