namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addproblemandrequeststatusesenums : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Requests", "RequestStatus", c => c.Int(nullable: false));
            AddColumn("dbo.Problems", "ProblemStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Problems", "ProblemStatus");
            DropColumn("dbo.Requests", "RequestStatus");
        }
    }
}
