namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingconstraintstoQManagersclass : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.QualityManagers", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.QualityManagers", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.QualityManagers", "MailAddress", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.QualityManagers", "MailAddress", c => c.String());
            AlterColumn("dbo.QualityManagers", "LastName", c => c.String());
            AlterColumn("dbo.QualityManagers", "FirstName", c => c.String());
        }
    }
}
