namespace OTDM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changesinmoidel : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Devices", name: "DevCategoryId", newName: "DeviceCategoryId");
            RenameIndex(table: "dbo.Devices", name: "IX_DevCategoryId", newName: "IX_DeviceCategoryId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Devices", name: "IX_DeviceCategoryId", newName: "IX_DevCategoryId");
            RenameColumn(table: "dbo.Devices", name: "DeviceCategoryId", newName: "DevCategoryId");
        }
    }
}
