﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OTDM.Startup))]
namespace OTDM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
