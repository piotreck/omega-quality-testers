﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using OTDM.Models;
using OTDM.Dtos;

namespace OTDM.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Device, DeviceDto>();
            Mapper.CreateMap<DeviceDto, Device>();

            Mapper.CreateMap<Request, RequestDto>();
            Mapper.CreateMap<RequestDto, Request>();

            Mapper.CreateMap<QualityManager, QualityManagerDto>();
            Mapper.CreateMap<QualityManagerDto, QualityManager>();

            Mapper.CreateMap<DevCategory, DeviceCategoryDto>();
            Mapper.CreateMap<DeviceCategoryDto, DevCategory>();

        }
    }
}