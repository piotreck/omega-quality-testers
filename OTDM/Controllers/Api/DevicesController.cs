﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OTDM.Models;
using OTDM.Models.ViewModels;
using OTDM.Dtos;
using AutoMapper;
using System.Data.Entity;

namespace OTDM.Controllers.Api
{
    public class DevicesController : ApiController
    {
        private ApplicationDbContext _context;

        public DevicesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET /api/Devices
        public IHttpActionResult GetDevices()
        {
            return Ok(_context.Devices.Include(d =>d.DeviceCategory).ToList().Select(Mapper.Map<Device, DeviceDto>));
        }
        //GET /api/Devices/id

        public IHttpActionResult GetDevice(int id)
        {
            var device = _context.Devices.Include(d => d.DeviceCategory).SingleOrDefault(d => d.Id == id);
            if (device == null)
                return NotFound();
            else
                return Ok(Mapper.Map<Device, DeviceDto>(device));
        }

        //POST /api/Devices/
        [HttpPost]
        public IHttpActionResult CreateDevice(DeviceDto deviceDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _context.Devices.Add(Mapper.Map<DeviceDto, Device>(deviceDto));
            _context.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + deviceDto.Id), deviceDto);
        }   
            
        //PUT /api/Devices/id
        [HttpPut]
        public IHttpActionResult UpdateDevice(DeviceDto deviceDto, int id)
        {
            if (!ModelState.IsValid)
            {
                BadRequest();
            }
            else
            {
                var deviceInDb = _context.Devices.SingleOrDefault(d => d.Id == id);
                if (deviceInDb == null)
                {
                    NotFound();
                }
                Mapper.Map(deviceDto, deviceInDb);

                _context.SaveChanges();
              
            }

            return Ok((Request.RequestUri + "/" + id));
        }

        //DELETE /api/Devices/id
        [HttpDelete]
        public IHttpActionResult DeleteDevice(int id)
        {
            var device = _context.Devices.Single(d => d.Id == id);

            _context.Devices.Remove(device);
            _context.SaveChanges();
            return Ok("Removed");
        }
    }
}
