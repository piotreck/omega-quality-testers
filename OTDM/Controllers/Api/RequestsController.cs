﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OTDM.Models;
using OTDM.Dtos;
using AutoMapper;
using OTDM.Models.ViewModels;

namespace OTDM.Controllers.Api
{
    public class RequestsController : ApiController
    {
        private ApplicationDbContext _context;

        public RequestsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET /api/Requests
        public IEnumerable<RequestDto> GetRequests()
        {
            return _context.Requests.ToList().Select(Mapper.Map<Request, RequestDto>);
        }

        //GET /api/Requests/id

        public RequestDto GetRquest(int id)
        {
            var request = _context.Requests.SingleOrDefault(r => r.Id == id);
            if (request == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return Mapper.Map<Request, RequestDto>(request);

        }

        //POST /api/Requests
        [HttpPost]
        public RequestDto CreateRequest(RequestDto requestDto)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            _context.Requests.Add(Mapper.Map<RequestDto, Request>(requestDto));
            _context.SaveChanges();
            return requestDto;      
        }

        //PUT /api/Request/id
        [HttpPut]
        public void UpdateRequest(int id, RequestDto requestDto)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            else
            {
                var request = _context.Requests.SingleOrDefault(r => r.Id == id);
                if (request == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                Mapper.Map(request, requestDto);
                _context.SaveChanges();
            }

        }

        //Delete /api/Request/id
        [HttpDelete]
        public void RemoveRequest(int id)
        {
            var request = _context.Requests.SingleOrDefault(r => r.Id == id);
            _context.Requests.Remove(request);
            _context.SaveChanges();
        }
    }
}
