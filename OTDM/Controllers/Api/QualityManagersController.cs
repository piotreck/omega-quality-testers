﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OTDM.Models;
using OTDM.Dtos;
using AutoMapper;

namespace OTDM.Controllers.Api
{
    public class QualityManagersController : ApiController
    {
        private ApplicationDbContext _context;

        public QualityManagersController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        //GET /api/QualityManagers/
        public IHttpActionResult GetQualityManagers()
        {
            return Ok(_context.QualityManagers.ToList().Select(Mapper.Map<QualityManager, QualityManagerDto>));
        }


        //GET /api/QualityManagers/id
        public IHttpActionResult GetQualityManager(int id)
        {
            var qualityManager = _context.QualityManagers.SingleOrDefault(q => q.Id == id);
            if (qualityManager == null)
            {
               return NotFound();
            }
                
            return Ok(Mapper.Map<QualityManager, QualityManagerDto>(qualityManager));
        }

        //POST /api/QualityManagers/
        [HttpPost]
        public IHttpActionResult CreateQualityManager(QualityManagerDto qualityManagerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _context.QualityManagers.Add(Mapper.Map<QualityManagerDto, QualityManager>(qualityManagerDto));
            _context.SaveChanges();
            var qualityManager = Mapper.Map<QualityManagerDto, QualityManager>(qualityManagerDto);
            
            return Created(new Uri(Request.RequestUri + "/" + qualityManagerDto.Id), qualityManagerDto);
        }

        //PUT /api/QualityManagers/id
        [HttpPut]
        public IHttpActionResult UpdateQualityManager(int id, QualityManagerDto qualityManagerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var qualityManagerInDb = _context.QualityManagers.SingleOrDefault(q => q.Id == id);
            if (qualityManagerInDb == null)
            {
                return NotFound();
            }
            Mapper.Map(qualityManagerDto, qualityManagerInDb);
            _context.SaveChanges();

            return Ok();

        }

        //DELETE /api/QualityManagers/id
        [HttpDelete]
        public IHttpActionResult RemoveQualityManager(int id)
        {
            _context.QualityManagers.Remove(_context.QualityManagers.SingleOrDefault(q => q.Id == id));
            _context.SaveChanges();
            return Ok();
        }
    }
}
