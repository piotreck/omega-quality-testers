﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OTDM.Models;
using System.Data.Entity;
using OTDM.Models.ViewModels;
using Microsoft.AspNet.Identity.Owin;


namespace OTDM.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.QualityManager + "," + RoleName.TechnicalInvestigator + "," + RoleName.Auditor)]
    public class DevicesController : System.Web.Mvc.Controller
    {
        private ApplicationDbContext _context;
        private ApplicationUserManager _userManager;


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public DevicesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [Authorize(Roles = RoleName.Admin)]
        public ActionResult New()
        {
            var devCategories = _context.DevCategories.ToList();
            var viewModel = new DeviceFormViewModel
            {
                DevCategoriesList = devCategories,
                Device = new Device()
            };

            return View("DeviceForm", viewModel);
        }
        [Authorize(Roles = RoleName.Admin)]
        public ActionResult Edit(int id)
        {
            var device = _context.Devices.SingleOrDefault(d => d.Id == id);
            var viewModel = new DeviceFormViewModel
            {
                Device = device,
                DevCategoriesList = _context.DevCategories.ToList()
            };
            return View("DeviceForm", viewModel);
            ; }

        [Authorize(Roles = RoleName.Admin)]
        public ActionResult Remove(int id)
        {
            var device = _context.Devices.Single(d => d.Id == id);
            _context.Devices.Remove(device);
            _context.SaveChanges();
            return RedirectToAction("Index", "Devices");
            
        }

        [Authorize(Roles = RoleName.Admin)]
        [HttpPost]
        public ActionResult Save(Device device)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new DeviceFormViewModel
                {
                    Device = device,
                    DevCategoriesList = _context.DevCategories.ToList()

                };
                return View("DeviceForm", viewModel);
            }
            if (device.Id == 0)
            {
                _context.Devices.Add(device);
            }
            else
            {
                var deviceInDb = _context.Devices.Single(d => d.Id == device.Id);
                deviceInDb.Name = device.Name;
                deviceInDb.Code = device.Code;
                deviceInDb.NextValidationDate = device.NextValidationDate;
                deviceInDb.IsValidated = device.IsValidated;
                deviceInDb.DeviceCategoryId = device.DeviceCategoryId;

            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Devices");
        }
        
        public ActionResult Index()
        {
            var viewModel = new DevicesIndexViewModel
            {
                DevicesList = _context.Devices.Include(d => d.DeviceCategory).ToList(),
                RequestsList = _context.Requests.Include(r => r.Requester).Where(r=>r.RequestStatus!=RequestStatusEnum.Completed).ToList(),
                ViewUserManager = UserManager
            };    
            
            if (User.IsInRole(RoleName.Admin))
                return View("Index_Admin", viewModel);
            else if (User.IsInRole(RoleName.Auditor))
                return View("Index_Auditor", viewModel);
            else return View("Index_Requester", viewModel);
        }
        
        public ActionResult Details(int? id)
        {
            if (id == null || !_context.Devices.ToList().Exists(d => d.Id == id))
            {
                return Content("Id has not been provided or Device with provided id does not exist");
            }
            var device = _context.Devices.SingleOrDefault(d => d.Id == id);
            return View(device);
        }

        public ActionResult DeviceHistory(int id)
        {
            var viewModel = new DeviceHistoryViewModel
            {
                RequestsList = _context.Requests.Where(r => r.RequestedDeviceId == id).Include(r=>r.RequestedDevice).ToList(),
                ProblemsList = _context.Problems.Where(p => p.DeviceId == id).Include(p=>p.Device).ToList()
            };

            return View(viewModel);
        }

        

        /*public ActionResult Unblock(int id)
        {
            var deviceInDb = _context.Devices.SingleOrDefault(d => d.Id == id);
            if(deviceInDb.IsAvailable == true)
                throw new InvalidOperationException("Device cannot be unblocked, because it is not in use at the moment");
            else
            {
                deviceInDb.IsAvailable = true;
                deviceInDb.UserId = null;
            }
            _context.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult Unblock2(int id)
        {
            var request = _context.Requests.SingleOrDefault(d => d.Id == id);
            var deviceInDb = request.RequestedDevice;
            request.EndDate = DateTime.Now;
            if (deviceInDb.IsAvailable == true)
                throw new InvalidOperationException("Device cannot be unblocked, because it is not in use at the moment");
            else
            {
                deviceInDb.IsAvailable = true;
                deviceInDb.UserId = null;
            }
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        /* public ActionResult Reassign(int id)
         {
             if (_context.Devices.SingleOrDefault(d => d.Id == id).UserId != User.Identity.GetUserId())
                 throw new UnauthorizedAccessException("Nie jestes upowazniony do tej operacji");
             else
                 _context.Devices.SingleOrDefault(d => d.Id == id).UserId ;
             return View("ReassignForm",)
         }*/

    }
}