﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OTDM.Models;
using OTDM.Models.ViewModels;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Data.Entity;

namespace OTDM.Controllers
{
    [Authorize(Roles = RoleName.Admin)]
    public class PermissionsController : Controller
    {
        private ApplicationDbContext _context;
        private ApplicationUserManager _userManager;


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public PermissionsController()
        {
            _context = new ApplicationDbContext();
        }

        public PermissionsController(ApplicationUserManager userManager)
        {
            _context = new ApplicationDbContext();
            UserManager = userManager;
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Permissions
        public ActionResult AllPermissions()
        {

            var allUsers = _context.Users.ToList();
            var users = allUsers
                        .Select(u => new PermissionViewModel
                        {
                            User = u,
                            RolesList = _context.Roles
                            .Where(role => role.Users.Any(user => user.UserId == u.Id))
                            .ToList()
                        });
                  

            return View(users);

        }

        public ActionResult ManagePermissions(string id)
        {
            if (id == null)
                return RedirectToAction("AllPermissions");

            var viewModel = new PermissionViewModel
            {
                User = _context.Users.SingleOrDefault(u => u.Id == id),
                RolesList = _context.Roles.ToList(),
                RolesCheckBoxes = new Dictionary<string, bool>()
            };
            
            foreach(var role in viewModel.RolesList)
            {
                viewModel.RolesCheckBoxes.Add(role.Name, viewModel.User.Roles.Any(r => r.RoleId == role.Id));
            }


            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Update (PermissionViewModel viewModel)
        {

            var formCheckboxes = viewModel.RolesCheckBoxes;
            foreach (var checkbox in formCheckboxes)
            {
                if (checkbox.Value == true)
                {
                    UserManager.AddToRole(viewModel.User.Id, checkbox.Key);
                }

                else
                {
                    UserManager.RemoveFromRole(viewModel.User.Id, checkbox.Key);
                }

            }
            return RedirectToAction("AllPermissions");
        }
    }
}