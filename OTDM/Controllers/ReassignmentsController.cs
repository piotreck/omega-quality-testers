﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OTDM.Models;
using OTDM.Models.ViewModels;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace OTDM.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.QualityManager + "," + RoleName.Auditor + "," + RoleName.TechnicalInvestigator)]
    public class ReassignmentsController : Controller
    {
        private ApplicationDbContext _context;
        private ApplicationUserManager _userManager;


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ReassignmentsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Reassignments
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.QualityManager)]
        public ActionResult ReassignDeviceForm(int id)
        {
            if (_context.Requests.Any(r => r.Id == id && r.RequestStatus == RequestStatusEnum.Completed))
                throw new InvalidOperationException("Niepoprawna operacja");

            else if (_context.Requests.SingleOrDefault(r => r.Id == id).RequesterId != User.Identity.GetUserId() && !User.IsInRole(RoleName.Admin))
                throw new InvalidOperationException("Nie masz uprawnień do przekazania tego testera (nie jesteś w jego posiadaniu, lub nie masz uprawnień administratora)");

            var viewModel = new ReassignFormViewModel
            {
                Reassignment = new Reassignment
                {
                    RequestId = id,
                    ReassignmentDate = DateTime.Now,
                    ReassignedFrom = _context.Requests.SingleOrDefault(r=>r.Id==id).RequesterId
                },

                RequestersList = _context.Users.ToList()
            };
            return View(viewModel);
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.QualityManager)]
        public ActionResult CreateReassignment(ReassignFormViewModel viewModel)
        {
            if (_context.Requests.Any(r => r.Id == viewModel.Reassignment.RequestId && r.RequestStatus == RequestStatusEnum.Completed))
                throw new InvalidOperationException("Niepoprawna operacja");
            else if (_context.Requests.SingleOrDefault(r => r.Id == viewModel.Reassignment.RequestId).RequesterId != User.Identity.GetUserId() && !User.IsInRole(RoleName.Admin))
                throw new InvalidOperationException("Nie masz uprawnień do przekazania tego testera (nie jesteś w jego posiadaniu, lub nie masz uprawnień administratora)");

            else if (!ModelState.IsValid)
            {
                var validatedViewModel = new ReassignFormViewModel();
                validatedViewModel = viewModel;
                return View("ReassignDeviceForm", viewModel);
               
            }
            else
            {
                _context.Reassignments.Add(viewModel.Reassignment);
                _context.Requests.SingleOrDefault(r => r.Id == viewModel.Reassignment.RequestId).RequesterId = viewModel.Reassignment.ReassignedTo;
                _context.SaveChanges();
                return RedirectToAction("MyActivities", "Activities");
            }
        }

        public ActionResult ReassignmentsHistory(int id)
        {
            var viewModel = new ReassignmentsHistoryViewModel
            {
                ReassignmentsList = _context.Reassignments.Where(r => r.RequestId == id).ToList(),
                ViewUserManager = UserManager

            };
            
            return View(viewModel);
        }
    }
}