﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OTDM.Models;

namespace OTDM.Controllers
{
    public class QualityManagersController : Controller
    {
        private ApplicationDbContext _context;
        public QualityManagersController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult Index()
        {
            var qManagersList = _context.QualityManagers.ToList();
            return View(qManagersList);
        }

        public ActionResult Details(int id)
        {
            var qualityManager = _context.QualityManagers.SingleOrDefault(q => q.Id == id);
            return View(qualityManager);
        }

        public ActionResult New()
        {
            var qManager = new QualityManager();

            return View("QualityManagerForm", qManager);
        }

        public ActionResult Edit(int id)
        {
            var qmanager = _context.QualityManagers.SingleOrDefault(qm => qm.Id == id);
            return View("QualityManagerForm", qmanager);
        }

        public ActionResult Remove(int id)
        {
            var qManager = _context.QualityManagers.Single(qm => qm.Id == id);
            _context.QualityManagers.Remove(qManager);
            _context.SaveChanges();
            return RedirectToAction("Index", "QualityManagers");
        }

        [HttpPost]
        public ActionResult Save(QualityManager qmanager)
        {
            if (!ModelState.IsValid)
            {
                return View("QualityManagerForm", qmanager);
            }
            else
            if (qmanager.Id == 0)
            {
                _context.QualityManagers.Add(qmanager);
            }
            else
            {
                var qManagerInDb = _context.QualityManagers.Single(qm => qm.Id == qmanager.Id);
                qManagerInDb.FirstName = qmanager.FirstName;
                qManagerInDb.LastName = qmanager.LastName;
                qManagerInDb.MailAddress = qmanager.MailAddress;
                qManagerInDb.PhoneNumber = qmanager.PhoneNumber;
                qManagerInDb.Manager = qmanager.Manager;
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "QualityManagers");
        }

    }
}