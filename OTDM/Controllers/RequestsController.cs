﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OTDM.Models;
using OTDM.Models.ViewModels;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using OTDM.Util;

namespace OTDM.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.QualityManager + "," + RoleName.Auditor + "," + RoleName.TechnicalInvestigator)]
    public class RequestsController : Controller
    {

        private ApplicationDbContext _context;

        public RequestsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


        public ActionResult Index()
        {
            var requestsList = _context.Requests.Include("RequestedDevice").ToList();
            if (User.IsInRole(RoleName.Admin))
                return View("Index_Admin", requestsList);
            else if (User.IsInRole(RoleName.Auditor))
                return View("Index_Auditor", requestsList);
            else return View("Index_Requester", requestsList);
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.QualityManager)]
        public ActionResult RequestDevice(int? id)
        {

            if (id == null)
                return RedirectToAction("Index");

            else if (!_context.Devices.SingleOrDefault(d => d.Id == id).IsValidated || _context.Devices.SingleOrDefault(d=>d.Id == id).NextValidationDate <= DateTime.Now)
                throw new InvalidOperationException("Tester nie został jeszcze zwalidowany lub jest sparwdzany przez dział techniczny w związku ze zgłoszoniem problemu");

            else if (_context.Requests.Any(r => r.RequestedDeviceId == id && r.RequestStatus != RequestStatusEnum.Completed))
                throw new InvalidOperationException("Ten tester jest obecnie w użyciu");

            else
            {
                var viewModel = new RequestFormViewModel
                {
                    Request = new Request
                    {
                        RequestedDeviceId = (int)id,
                        RequesterId = User.Identity.GetUserId()
                    },     
                    
                    RequesterName = User.Identity.GetUserName(),
                    DeviceName = _context.Devices.SingleOrDefault(d => d.Id == id).Name

                };
                return View(viewModel);
            }      
            
        }
        [Authorize(Roles = RoleName.Admin + "," + RoleName.QualityManager)]
        public ActionResult CreateRequest(RequestFormViewModel viewModel)
        {
            var deviceInDb = _context.Devices.SingleOrDefault(d => d.Id == viewModel.Request.RequestedDeviceId);
            if ((_context.Requests.Any(r => r.RequestedDeviceId == viewModel.Request.RequestedDeviceId && r.RequestStatus != RequestStatusEnum.Completed)))
                throw new InvalidOperationException("Ten tester jest obecnie w użyciu");
            else if (!ModelState.IsValid)
            {
                var validatedViewModel = new RequestFormViewModel();
                validatedViewModel = viewModel;
                return View("RequestDevice", validatedViewModel);
            }

            _context.Requests.Add(viewModel.Request);
            _context.SaveChanges();

            MailSender.SendMail(GroupMails.ProductionGroupMail, GroupMails.QualityGroupMail, "Prośba o przygotowanie urządzenia",
                                viewModel.RequesterName + " poprosił o przygotowanie urządzenia: " + viewModel.DeviceName + " - " +viewModel.Request.RequestedDevice.Code + " w związku ze zleceniem produkcyjnym: " + viewModel.Request.OrderId +
                                ". Czas rozpoczęcia produkcji: " + viewModel.Request.StartingDate + ". Estymowany czas zakonczenia użytkowania: " + viewModel.Request.EstimatedUsageTime);

            return RedirectToAction("Index", "Devices");
        }

        [Authorize(Roles = RoleName.Admin)]
        public ActionResult Prepared(int id)
        {
            var requestInDb = _context.Requests.Include(r=>r.RequestedDevice).SingleOrDefault(r => r.Id == id);
            if (requestInDb.RequestStatus != RequestStatusEnum.WaitingForAction)
                throw new InvalidOperationException("Niepoprawna operacja");
            else
            {
                requestInDb.RequestStatus = RequestStatusEnum.WaitingForCollection;
                requestInDb.GiverId = User.Identity.GetUserId();
            }

            _context.SaveChanges();

            MailSender.SendMail(GroupMails.QualityGroupMail, GroupMails.ProductionGroupMail, "Urządzenie oczekuje na odbiór", 
                "Urządzenie: " + requestInDb.RequestedDevice.Name + " o kodzie: " + requestInDb.RequestedDevice.Code + " zamówione do zlecenia produkcyjnego: " + requestInDb.OrderId + " oczekuje na odbiór lidera.");

            return RedirectToAction("MyActivities", "Activities");
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.QualityManager)]
        public ActionResult Collected(int id)
        {
            if (_context.Requests.Any(r => r.Id == id && r.RequestStatus != RequestStatusEnum.WaitingForCollection))
                throw new InvalidOperationException("Niepoprawna operacja");
            else
            {
                var requestInDb = _context.Requests.SingleOrDefault(r => r.Id == id);
                requestInDb.RequestStatus = RequestStatusEnum.InUse;
                
            }
            _context.SaveChanges();
            return RedirectToAction("MyActivities", "Activities");
        }

        [Authorize(Roles = RoleName.Admin)]
        public ActionResult CommentForm(int id)
        {
            if (_context.Requests.Any(r => r.Id == id && r.RequestStatus == RequestStatusEnum.Completed))
                throw new InvalidOperationException("To zamówienie zostało już zakonczone");
            else
            {
                var viewModel = new RequestCompletetedCommentViewModel
                {
                    RequestId = id,

                };
                return View(viewModel);
            }
                
        }

        [Authorize(Roles = RoleName.Admin)]
        public ActionResult UnblockDevice(RequestCompletetedCommentViewModel viewModel)
        {
            if (_context.Requests.Any(r => r.Id == viewModel.RequestId && r.RequestStatus == RequestStatusEnum.Completed))
                throw new InvalidOperationException("To zamówienie zostało już zakonczone");
            else
            {
                var requestInDb = _context.Requests.SingleOrDefault(r => r.Id == viewModel.RequestId);
                requestInDb.RequestStatus = RequestStatusEnum.Completed;
                requestInDb.ReceiverId = User.Identity.GetUserId();
                requestInDb.EndDate = DateTime.Now;
                requestInDb.Comment = viewModel.Request.Comment;

                _context.SaveChanges();

            }
            return RedirectToAction("MyActivities", "Activities");


        }

      
    }
}