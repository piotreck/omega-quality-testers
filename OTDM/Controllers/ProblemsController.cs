﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OTDM.Models;
using OTDM.Models.ViewModels;
using Microsoft.AspNet.Identity;
using OTDM.Util;
using System.Data.Entity;

namespace OTDM.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.QualityManager + "," + RoleName.TechnicalInvestigator + "," + RoleName.Auditor)]
    public class ProblemsController : Controller
    {
        private ApplicationDbContext _context;
        public ProblemsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Histories
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.QualityManager + "," + RoleName.TechnicalInvestigator)]
        public ActionResult ReportProblem(int id)
        {
            if (!_context.Devices.SingleOrDefault(d => d.Id == id).IsValidated)
                throw new InvalidOperationException("Ten tester nie został jeszcze dopuszcozny do użytku lub jest obecnie sprawdzany przez zespół techniczny");
            var viewModel = new ReportProblemViewModel
            {
               
                Problem = new Problem
                {
                    DeviceId = id,
                    ReportingDate = DateTime.Now,
                    ReporterId = User.Identity.GetUserId()
                },

                ReporterName = User.Identity.GetUserName(),
                ProblematicDeviceName = _context.Devices.SingleOrDefault(d => d.Id == id).Name
            };
            return View(viewModel);
        }


        [Authorize(Roles = RoleName.Admin + "," + RoleName.QualityManager + "," + RoleName.TechnicalInvestigator)]
        [HttpPost]
        public ActionResult CreateProblem (ReportProblemViewModel viewModel)
        {
            if (!_context.Devices.SingleOrDefault(d => d.Id == viewModel.Problem.DeviceId).IsValidated)
                throw new InvalidOperationException("Ten tester nie został jeszcze dopuszcozny do użytku lub jest obecnie sprawdzany przez zespół techniczny w związku ze zgłoszeniem");
            else if (!ModelState.IsValid)
            {
                return View("ReportProblem", viewModel);
            }

            _context.Problems.Add(viewModel.Problem);
            _context.Devices.SingleOrDefault(d => d.Id == viewModel.Problem.DeviceId).IsValidated = false;
            _context.SaveChanges();

            MailSender.SendMail(GroupMails.QualityGroupMail, GroupMails.Technician, "Problem z urządzeniem " + viewModel.Problem.Device.Code,
                                viewModel.ReporterName + " zgłosił problem z urządzeniem: " +viewModel.Problem.Device.Name + " - " + viewModel.Problem.Device.Code + ". " + "</br></br>" + "<b>Opis problemu: </b></br>" + "<i>" + viewModel.Problem.ProblemDescription + "</i>");

            return RedirectToAction("Index", "Devices");
        }

        public ActionResult ResolutionForm(int id)
        {
            if (_context.Problems.Any(p => p.Id == id && p.ProblemStatus == ProblemStatusEnum.Resolved))
                throw new InvalidOperationException("Ten problem został już rozwiązany");
            var viewModel = new ResolutionFormViewModel
            {
                ProblemId = id
            };
            return View(viewModel);
        }

        public ActionResult ResolveProblem(ResolutionFormViewModel viewModel)
        {
            if (_context.Problems.Any(p => p.Id == viewModel.ProblemId && p.ProblemStatus == ProblemStatusEnum.Resolved))
                throw new InvalidOperationException("Ten problem został już rozwiązany");
            var problemInDb = _context.Problems.Include(p=>p.Device).SingleOrDefault(p => p.Id == viewModel.ProblemId);
            var problematicDevice = _context.Devices.SingleOrDefault(d => d.Id == problemInDb.DeviceId);
            
            problemInDb.ProblemStatus = ProblemStatusEnum.Resolved;
            problemInDb.ResolverId = User.Identity.GetUserId();
            problemInDb.ResolutionDate = DateTime.Now;
            problemInDb.ResolutionDescription = viewModel.Problem.ResolutionDescription;
            problematicDevice.IsValidated = true;
            _context.SaveChanges();

            MailSender.SendMail(GroupMails.Technician, GroupMails.ProductionGroupMail + "," + GroupMails.QualityGroupMail, "Problem nr: " + problemInDb.Id + " został rozwiązany",
                "Problem z urządzeniem: " + problemInDb.Device.Name + " - " + problemInDb.Device.Code + " został rozwiązany. </br></br>" + "<b>Opis problemu: </b></br>" + "<i>" + problemInDb.ProblemDescription + "</i>.</br></br><b> Opis rozwiązania: </b></br>" + "<i>" + problemInDb.ResolutionDescription + ".</i>");

            return RedirectToAction("MyActivities", "Activities");
        }

        
    }
}