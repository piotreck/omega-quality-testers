﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OTDM.Models;
using OTDM.Models.ViewModels;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace OTDM.Controllers
{
    public class ActivitiesController : Controller
    {
        private ApplicationDbContext _context;

        public ActivitiesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Activities
        public ActionResult MyActivities()
        {
            IEnumerable<Request> myActiveTasks;
            var currentUser = User.Identity.GetUserId();
            var isAdmin = User.IsInRole(RoleName.Admin);
            var isQualityManager = User.IsInRole(RoleName.QualityManager);
            var isTechnicalInvestigator = User.IsInRole(RoleName.TechnicalInvestigator);

            if (isAdmin)
                myActiveTasks = _context.Requests.Where(r => r.RequestStatus != RequestStatusEnum.Completed && r.RequestStatus != RequestStatusEnum.WaitingForCollection && isAdmin).Include(r => r.RequestedDevice).ToList();
            else if (isQualityManager)
                myActiveTasks = _context.Requests.Where(r => r.RequestStatus == RequestStatusEnum.WaitingForCollection && r.RequesterId == currentUser && isQualityManager).Include(r => r.RequestedDevice).ToList();
            else
                myActiveTasks = Enumerable.Empty<Request>();
            var viewModel = new MyActivitiesViewModel
            {

                MyActiveTasks = myActiveTasks,
                MyArchivedTasks = _context.Requests.Where(r => r.GiverId == currentUser || r.ReceiverId == currentUser && r.RequestStatus == RequestStatusEnum.Completed).Include(r => r.RequestedDevice).ToList(),
                MyActiveRequests = _context.Requests.Where(r => r.RequestStatus != RequestStatusEnum.Completed && r.RequesterId == currentUser).Include(r => r.RequestedDevice).ToList(),
                MyArchivedRequests = _context.Requests.Where(r => r.RequestStatus == RequestStatusEnum.Completed && r.RequesterId == currentUser).Include(r => r.RequestedDevice).ToList(),
                ProblemsToResolve = _context.Problems.Where(p => p.ProblemStatus != ProblemStatusEnum.Resolved && (isAdmin || isTechnicalInvestigator)).Include(p=>p.Device).ToList(),
                ProblemsReportedByMe = _context.Problems.Where(p => p.ReporterId == currentUser).Include(p => p.Device).ToList()

            };
            return View(viewModel);
        }
    }
}