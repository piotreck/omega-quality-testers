﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OTDM.Models;

namespace OTDM.Controllers
{
    public class HistoriesController : Controller
    {
        private ApplicationDbContext _context;
        public HistoriesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Histories
        public ActionResult Index()
        {
            return View();
        }
    }
}