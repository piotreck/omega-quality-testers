﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OTDM.Dtos;

namespace OTDM.Models.Validations
{
    public class SringChecker : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var device = (DeviceDto)validationContext.ObjectInstance;
            if (device.Code == null || device.Code.Length != 6)
            {
                return new ValidationResult("ma byc równe 6");
            }
            else
                return ValidationResult.Success;
        }
    }
}