﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDM.Models
{
    public static class RoleName
    {
        public const string Admin = "Specjalista ds. jakosci";
        public const string QualityManager = "Lider";
        public const string TechnicalInvestigator = "Technik";
        public const string Auditor = "Audytor";
    }
}