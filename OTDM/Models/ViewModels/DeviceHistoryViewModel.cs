﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDM.Models.ViewModels
{
    public class DeviceHistoryViewModel
    {
        public IEnumerable<Request> RequestsList { get; set; }
        public IEnumerable<Problem> ProblemsList { get; set; }
    }
}