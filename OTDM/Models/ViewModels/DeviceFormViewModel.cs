﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDM.Models.ViewModels
{
    public class DeviceFormViewModel
    {
        public Device Device { get; set; }
        public IEnumerable<DevCategory> DevCategoriesList { get; set; }

    }
}