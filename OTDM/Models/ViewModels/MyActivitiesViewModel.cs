﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDM.Models.ViewModels
{
    public class MyActivitiesViewModel
    {
        public IEnumerable<Request> MyActiveTasks { get; set; }
        public IEnumerable<Request> MyArchivedTasks { get; set; }
        public IEnumerable<Request> MyActiveRequests { get; set; }
        public IEnumerable<Request> MyArchivedRequests { get; set; }
        public IEnumerable<Problem> ProblemsToResolve { get; set; }
        public IEnumerable<Problem> ProblemsReportedByMe { get; set; }

    }
}