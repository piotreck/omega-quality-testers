﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace OTDM.Models.ViewModels
{
    public class RequestFormViewModel
    {
        public Request Request { get; set; }
        public ApplicationUser Requester { get; set; }

        [Display(Name = "Imię i nazwisko zamawiającego")]
        public string RequesterName { get; set;  }

        [Display(Name = "Nazwa urządzenia")]
        public string DeviceName { get; set; }
    }
}