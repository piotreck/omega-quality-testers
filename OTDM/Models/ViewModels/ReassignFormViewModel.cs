﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDM.Models.ViewModels
{
    public class ReassignFormViewModel
    {
        public Reassignment Reassignment { get; set; }
        public IEnumerable<ApplicationUser> RequestersList { get; set; }
    }
}