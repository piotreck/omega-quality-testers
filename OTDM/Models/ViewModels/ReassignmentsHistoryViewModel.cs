﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDM.Models.ViewModels
{
    public class ReassignmentsHistoryViewModel
    {
        public IEnumerable<Reassignment> ReassignmentsList { get; set; }
        public ApplicationUserManager ViewUserManager { get; set; }
    }
}