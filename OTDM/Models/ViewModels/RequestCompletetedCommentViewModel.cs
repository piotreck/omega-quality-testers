﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDM.Models.ViewModels
{
    public class RequestCompletetedCommentViewModel
    {
        public int RequestId { get; set; }
        public Request Request { get; set; }
    }
}