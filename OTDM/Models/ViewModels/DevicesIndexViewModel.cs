﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OTDM.Models;

namespace OTDM.Models.ViewModels
{
    public class DevicesIndexViewModel
    {
        public IEnumerable<Device> DevicesList { get; set; }
        public IEnumerable<Request> RequestsList { get; set; }
        public ApplicationUserManager ViewUserManager { get; set; }
    }
}