﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace OTDM.Models.ViewModels
{
    public class ReportProblemViewModel
    {
        public Problem Problem { get; set; }

        [Display(Name = "Imię i nazwisko zgłaszającego")]
        public string ReporterName { get; set; }

        [Display(Name = "Nazwa urządzenia")]
        public string ProblematicDeviceName { get; set; }
    }
}