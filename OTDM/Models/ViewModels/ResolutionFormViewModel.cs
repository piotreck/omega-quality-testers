﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDM.Models.ViewModels
{
    public class ResolutionFormViewModel
    {
        public int ProblemId { get; set; }
        public Problem Problem { get; set; }
    }
}