﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace OTDM.Models.ViewModels
{
    public class PermissionViewModel
    {

            [Display(Name = "Użytkownik")]
            public ApplicationUser User { get; set; }

            [Display(Name = "Role")]
            public IEnumerable<IdentityRole> RolesList { get; set; }
           
            public Dictionary<string,bool> RolesCheckBoxes { get; set; }
    }
}