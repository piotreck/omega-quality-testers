﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace OTDM.Models
{
    public class Request
    {
        public int Id { get; set; }

        public ApplicationUser Requester { get; set; }

        [Required]
        public string RequesterId { get; set; }

        [Display(Name ="Użytkownicy produkcyjni")]
        public string ProductionUser { get; set; }

        public ApplicationUser Giver { get; set; }
        public string GiverId { get; set; }

        [Required]
        [Display(Name = "Estymowany czas zwrotu")]
        public DateTime? EstimatedUsageTime { get; set; }

        public ApplicationUser Receiver { get; set; }
        public string ReceiverId { get; set; }

        [Required]
        [Display(Name = "Numer zamówienia produkcyjnego")]
        public string OrderId { get; set; }
        
        public Device RequestedDevice { get; set; }

        [Required]
        public int RequestedDeviceId { get; set; }

        [Required]
        [Display(Name = "Data rozpoczęcia produkcji")]
        public DateTime? StartingDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Comment { get; set; }

        public RequestStatusEnum RequestStatus { get; set; } = RequestStatusEnum.WaitingForAction;
        
    }

    public enum RequestStatusEnum
    {
        WaitingForAction = 1,
        WaitingForCollection = 2,
        InUse = 3,
        Completed = 4
    }

}