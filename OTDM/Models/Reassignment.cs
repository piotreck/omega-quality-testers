﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace OTDM.Models
{
    public class Reassignment
    {
        public int Id { get; set; }
        public int RequestId { get; set; }
        public Request Request { get; set; }
        public DateTime ReassignmentDate { get; set; }

        
        public string ReassignedFrom { get; set; }

        [Display(Name = "Odbiorca urządzenia")]
        public string ReassignedTo { get; set; }

        [Display(Name = "Komentarz")]
        public string Comment { get; set; }
    }
}