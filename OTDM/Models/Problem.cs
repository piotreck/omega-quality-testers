﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace OTDM.Models
{
    public class Problem
    {
        public int Id { get; set; }

        public int DeviceId { get; set; }
        public Device Device { get; set; }

        public string ReporterId { get; set; }
        public ApplicationUser Reporter { get; set; }

        [Display(Name = "Opis problemu")]
        public string ProblemDescription { get; set; }

        [Display(Name = "Data zgłoszenia")]
        public DateTime ReportingDate { get; set; }

        public string ResolverId { get; set; }
        public ApplicationUser Resolver { get; set; }
        public string ResolutionDescription { get; set; }
        public DateTime? ResolutionDate { get; set; }

        public ProblemStatusEnum ProblemStatus { get; set; } = ProblemStatusEnum.WaitingForFix;
     
    }

    public enum ProblemStatusEnum
    {
        WaitingForFix=1,
        Resolved=2
    }
}