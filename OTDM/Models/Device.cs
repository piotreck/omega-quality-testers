﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using OTDM.Models.Validations;

namespace OTDM.Models
{
    public class Device
    {
        public int Id                           { get; set; }

        [Required]
        [Display(Name="Nazwa urządzenia")]
        public string Name                      { get; set; }

        [Required]
        //[SringChecker]
        [Display(Name="Kod urządzenia")]
        public string Code                      { get; set; }

       
        public DevCategory DeviceCategory       { get; set; }

        //[Required(ErrorMessage = "Please provide category of this device")]
        [Display(Name="Kategoria urządzenia")]
        public int DeviceCategoryId             { get; set; }

        public bool IsValidated                 { get; set; }

        [Required]
        [Display(Name="Data następnej walidacji")]
        public DateTime? NextValidationDate      { get; set; }

        public bool IsAvailable                 { get; set; } = true;

        public string UserId                    { get; set; }

        public ApplicationUser User             { get; set; }
    }
}