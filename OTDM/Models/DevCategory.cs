﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDM.Models
{
    public class DevCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}